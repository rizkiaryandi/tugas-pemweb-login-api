<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $user = new \App\User;
        $user->name = "Rizki Aryandi";
        $user->email = "me@rizkiaryandi.me";
        $user->password = Hash::make("password");
        $user->save();
    }
}
